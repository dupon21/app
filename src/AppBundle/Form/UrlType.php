<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UrlType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('url', 'text', array(
            'label' => 'form.url.url'
        ));

        $builder->setRequired(false);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_url';
    }
}