<?php
namespace AppBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\RouteCollection;

class LocaleRoutingLoader extends Loader
{
    /**
     * @param mixed $resource
     * @param null $type
     *
     * @return RouteCollection
     */
    public function load($resource, $type = null)
    {
        $collection = new RouteCollection();

        $type = 'yaml';

        /** @var RouteCollection $importedRoutes */
        $importedRoutes = $this->import($resource, $type);

        /** @var \Symfony\Component\Routing\Route $routes */
        $routes = $importedRoutes->getIterator();

        /** @var \Symfony\Component\Routing\Route $route */
        foreach ($collection as $route) {
            $route->addDefaults(array('_locale' => 'en'));
            $path = $route->getPath();
            $path = substr($path, 1);
            if (!$path) {
                $path = '';
            }
            $path = '/{_locale}/' . $path;

            $route->setPath($path);
        }

        $collection->addCollection($importedRoutes);

        return $collection;
    }

    /**
     * @param mixed $resource
     * @param null $type
     * @return bool
     */
    public function supports($resource, $type = null)
    {
        return $type === 'locale_route';
    }
}
