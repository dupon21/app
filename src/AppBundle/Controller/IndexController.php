<?php

namespace AppBundle\Controller;

use AppBundle\Utils\TokenGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\UrlType;

class IndexController extends Controller
{
    /**
     * Index page
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $manager = $this->get('app.url_manager');

        $url = $manager->createUrl();
        $form = $this->createForm(new UrlType(), $url);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $redis = $this->container->get('snc_redis.default');
                $existUrl = $redis->get($url->getUrl());

                if (!$existUrl) {
                    $existUrl = $manager->findUrlByUrl($url->getUrl());
                    if (!$existUrl) {
                        $token = TokenGenerator::generate();
                        $url->setToken($token);
                        $manager->updateUrl($url);
                        $redis->setex($url->getUrl(), 86400, $token);

                        return $this->redirect($this->generateUrl('app_url', array(
                            'token' => $url->getToken()
                        )));
                    }
                    $redis->setex($url->getUrl(), 86400, $url->getToken());

                    return $this->redirect($this->generateUrl('app_url', array(
                        'token' => $existUrl->getToken()
                    )));
                }

                return $this->redirect($this->generateUrl('app_url', array(
                    'token' => $existUrl
                )));
            }
        }

        return $this->render('AppBundle:Index:index.html.twig', array(
            'url' => $url,
            'form'   => $form->createView())
        );
    }

    /**
     * Returns url for showing
     *
     * @param $token
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($token)
    {
        $manager = $this->get('app.url_manager');

        $url = $manager->findUrlByToken($token);

        if (!$url) {
            throw $this->createNotFoundException($this->get('translator')->trans('message.not_found'));
        }

        return $this->render('AppBundle:Index:show.html.twig', array(
            'url' => $url
        ));
    }

    /**
     * Redirecting to url by token
     *
     * @param $token
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectAction($token)
    {
        $manager = $this->get('app.url_manager');
        $url = $manager->findUrlByToken($token);

        if (!$url) {
            throw $this->createNotFoundException($this->get('translator')->trans('message.not_found'));
        }

        return $this->redirect($url->getUrl());
    }
}