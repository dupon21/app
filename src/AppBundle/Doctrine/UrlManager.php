<?php
namespace AppBundle\Doctrine;

use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Url;

class UrlManager
{
    protected $objectManager;
    protected $class;
    protected $repository;

    /**
     * Constructor.
     *
     * @param ObjectManager           $om
     * @param string                  $class
     */
    public function __construct(ObjectManager $om, $class)
    {
        $this->objectManager = $om;
        $this->repository = $om->getRepository($class);
        $metadata = $om->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    /**
     * @param array $criteria
     *
     * @return object
     */
    public function findUrlBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * Returns an empty Url instance
     */
    public function createUrl()
    {
        $class = $this->getClass();
        $url = new $class;

        return $url;
    }

    /**
     * Updates an Url entity.
     *
     * @param Url $url
     */
    public function updateUrl(Url $url)
    {
        $this->objectManager->persist($url);
        $this->objectManager->flush();
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param $token
     *
     * @return mixed
     */
    public function findUrlByToken($token)
    {
        return $this->findUrlBy(array('token' => $token));
    }

    /**
     * @param $url
     *
     * @return mixed
     */
    public function findUrlByUrl($url)
    {
        return $this->findUrlBy(array('url' => $url));
    }
}